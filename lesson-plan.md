# Git Workshop Outline
## Purpose
### Not an exhaustive survey of Git's functionality
### Most typical workflow commands
### `git help`

## Installation and Setup

### Installation
#### Windows
##### GitPad

### Configuration
#### `git config --global user.name`
#### `git config --global user.email`
#### custom editor
#### CRLF handling
#### color to auto

### ssh-keygen
#### ssh-keygen included in GitHub package for Windows
#### generating public/private keypair
#### uploading public key to bitbucket.org

## Starting with a repository
### `git init .`
### `git clone <url>`

## Gathering information
### `git status`
### `git diff`
### `git log`
#### expand on `git log` options
##### `git log -p <ref>`
#### visualizing branches

## Working with files
### Staging files
### `git add <file>`
### Key point: `git add` does not add subsequent changes.
### `git diff --staged`
### `git diff HEAD`
### `git commit`
### `git rm <file>`
### `git mv <src-file> <dest-file>`
### Key point: all of this is done local

## Taking off some rough edges
### Adding patterns to .gitignore
### `git config alias.<name>`

## Branching
### Describe branching (slide 363)
### Value of light weight branching
### `git branch`
### `git branch <name>`
### `git checkout <branch>`
### `git checkout -b <branch>`
### `git merge`
### Resolving conflicts on merge
### `git branch -d <branch>`

## Syncing with the upstream repository
### (slide 264)
### `git push`
### `git pull`

## Working with GitHub/BitBucket
### Forking a repository
### Working on a fork
### Pull requests

## Refining workflow
### Discuss rebasing (slides 472-473)
### DON'T REBASE PREVIOUSLY PUSHED CHANGES!
### `git pull --rebase`
### `git rebase -i`
### `git stash`
### `git stash list`
### `git stash pop`
### `git stash branch <new-branch>`

## Fancy-pants UIs
### Your IDE most likely supports Git
### Host of GUIs available at http://git-scm/downloads/guis
